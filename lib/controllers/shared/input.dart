import 'package:mobx/mobx.dart';
part 'input.g.dart';

class InputController = _InputControllerBase with _$InputController;

abstract class _InputControllerBase with Store {
  @observable
  String value = '';

  @action
  setValue(String newValue) => value = newValue;

  @action
  _InputControllerBase({String? initialValue}) {
    value = initialValue ?? '';
  }
}
