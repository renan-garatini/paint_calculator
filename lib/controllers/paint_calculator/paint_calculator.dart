import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
import 'package:paint_calculator/controllers/shared/input.dart';
part 'paint_calculator.g.dart';

class PaintCalculatorController = _PaintCalculatorControllerBase
    with _$PaintCalculatorController;

class WallData {
  var widthController = InputController();
  var heightController = InputController();
  var windowController = InputController(initialValue: '0');
  var doorController = InputController(initialValue: '0');
}

abstract class _PaintCalculatorControllerBase with Store {
  List<WallData> walls = [WallData(), WallData(), WallData(), WallData()];

  var formKey = GlobalKey<FormState>();

  num doorHeight = 1.9;
  num doorWidth = 0.8;

  num windowHeight = 1.2;
  num windowWidth = 2;

  num paintYield = 5;

  List<num> paintCans = [0.5, 2.5, 3.6, 18];

  @observable
  String? errorMessage;

  @observable
  Map<num, int> paintResults = {};

  String? validate(String? value) {
    if (value == null || value.isEmpty) {
      return 'Please fill this field';
    } else if (num.tryParse(value) == null) {
      return 'Please enter a valid number';
    }
    return null;
  }

  String? validateDoorWall(String? value) {
    if (value == null || value.isEmpty) {
      return 'Please fill this field';
    } else if (int.tryParse(value) == null) {
      return 'Please enter a valid number';
    }
    return null;
  }

  String? validateWall(WallData wall) {
    num height = num.parse(wall.heightController.value);
    num width = num.parse(wall.widthController.value);

    num doorQty = num.parse(wall.doorController.value);
    num windowQty = num.parse(wall.windowController.value);

    num doorArea = doorHeight * doorWidth;
    num windowArea = windowHeight * windowWidth;

    num area = (height * width);

    num noPaintArea = ((doorQty * doorArea) + (windowQty * windowArea));

    if (area < 1 || area > 15) {
      return 'Please enter a valid wall size';
    }

    if ((area * 0.5) < noPaintArea) {
      return 'Sorry, you need to remove some doors or windows';
    }

    if (doorQty > 0 && (doorHeight + 0.3) > height) {
      return 'Wall height is too short';
    }

    return null;
  }

  num calculateTotalPaintArea(WallData wall) {
    num wallArea = num.parse(wall.heightController.value) *
        num.parse(wall.widthController.value);

    num totalDoorArea =
        num.parse(wall.doorController.value) * (doorHeight * doorWidth);
    num totalWindowArea =
        num.parse(wall.windowController.value) * (windowHeight * windowWidth);

    return wallArea - (totalDoorArea + totalWindowArea);
  }

  chooseCan(num can) {
    paintResults[can] = paintResults[can] == null ? 1 : paintResults[can]! + 1;
  }

  calculatePaintCans(num totalPaintArea) {
    num litersNeeded = (totalPaintArea / paintYield);

    //guarantees the ordering
    paintCans.sort();

    bool findCan;
    while (litersNeeded >= 0) {
      findCan = false;
      for (var can in paintCans.reversed) {
        if (litersNeeded - can >= 0) {
          litersNeeded -= can;
          chooseCan(can);
          findCan = true;
          break;
        }
      }

      //choose the can with the least amount over
      if (!findCan) {
        var lastCan = paintCans.reversed.last;
        chooseCan(lastCan);
        litersNeeded -= lastCan;
      }
    }
  }

  @action
  calculate() {
    paintResults = {};
    errorMessage = null;
    if (formKey.currentState!.validate()) {
      for (var wall in walls) {
        var wallError = validateWall(wall);
        if (wallError != null) {
          errorMessage = wallError;
          return;
        }
      }

      num totalPaintArea = 0;

      for (var wall in walls) {
        totalPaintArea += calculateTotalPaintArea(wall);
      }

      calculatePaintCans(totalPaintArea);
    }
  }
}
