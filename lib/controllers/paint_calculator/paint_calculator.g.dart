// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'paint_calculator.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$PaintCalculatorController on _PaintCalculatorControllerBase, Store {
  final _$errorMessageAtom =
      Atom(name: '_PaintCalculatorControllerBase.errorMessage');

  @override
  String? get errorMessage {
    _$errorMessageAtom.reportRead();
    return super.errorMessage;
  }

  @override
  set errorMessage(String? value) {
    _$errorMessageAtom.reportWrite(value, super.errorMessage, () {
      super.errorMessage = value;
    });
  }

  final _$paintResultsAtom =
      Atom(name: '_PaintCalculatorControllerBase.paintResults');

  @override
  Map<num, int> get paintResults {
    _$paintResultsAtom.reportRead();
    return super.paintResults;
  }

  @override
  set paintResults(Map<num, int> value) {
    _$paintResultsAtom.reportWrite(value, super.paintResults, () {
      super.paintResults = value;
    });
  }

  final _$_PaintCalculatorControllerBaseActionController =
      ActionController(name: '_PaintCalculatorControllerBase');

  @override
  dynamic calculate() {
    final _$actionInfo = _$_PaintCalculatorControllerBaseActionController
        .startAction(name: '_PaintCalculatorControllerBase.calculate');
    try {
      return super.calculate();
    } finally {
      _$_PaintCalculatorControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
errorMessage: ${errorMessage},
paintResults: ${paintResults}
    ''';
  }
}
