import 'package:flutter/material.dart';
import 'package:paint_calculator/theme.dart';

class Buttom extends StatelessWidget {
  final Function buttomAction;
  final String buttomText;

  const Buttom({Key? key, required this.buttomAction, required this.buttomText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
          padding: EdgeInsets.zero, primary: AppTheme.green),
      child: Text(
        buttomText,
        style: const TextStyle(
            fontWeight: FontWeight.bold, color: Colors.white, fontSize: 17),
      ),
      onPressed: () {
        buttomAction();
      },
    );
  }
}
