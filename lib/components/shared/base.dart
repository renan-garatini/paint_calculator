import 'package:flutter/material.dart';
import 'package:paint_calculator/theme.dart';

class Base extends StatelessWidget {
  final String screenName;
  final Widget body;
  const Base({Key? key, required this.screenName, required this.body})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      onPanDown: (_) => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize: const Size.fromHeight(40.0),
            child: AppBar(
              backgroundColor: AppTheme.orange,
              centerTitle: true,
              title: Text(screenName,
                  style: const TextStyle(fontWeight: FontWeight.bold)),
              elevation: 0,
            ),
          ),
          body: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                    border: Border.all(width: 0, color: AppTheme.orange),
                    color: AppTheme.orange),
                width: width,
                height: height * 0.07,
              ),
              ClipRRect(
                borderRadius: const BorderRadius.all(Radius.elliptical(45, 30)),
                child: Container(
                  width: width,
                  height: height,
                  decoration: const BoxDecoration(
                    color: Colors.white,
                  ),
                  child: SingleChildScrollView(
                    child: Padding(
                        padding: const EdgeInsets.only(top: 25.0, bottom: 25.0),
                        child: body),
                  ),
                ),
              )
            ],
          )),
    );
  }
}
