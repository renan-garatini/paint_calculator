import 'package:paint_calculator/theme.dart';

import '/controllers/shared/input.dart';
import 'package:flutter/material.dart';

class Input extends StatelessWidget {
  final InputController controller;
  final TextInputType type;
  final String? hintText;

  final String? Function(String?)? validate;

  const Input(
      {Key? key,
      required this.controller,
      required this.type,
      this.validate,
      this.hintText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: (value) {
        controller.setValue(value);
      },
      initialValue: controller.value,
      validator: validate ?? validate,
      keyboardType: type,
      obscureText: type == TextInputType.visiblePassword,
      decoration: InputDecoration(
          isDense: true,
          filled: true,
          hintText: hintText,
          fillColor: Colors.white,
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(width: 1, color: AppTheme.grey)),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(width: 1, color: AppTheme.grey)),
          hintStyle:
              TextStyle(fontWeight: FontWeight.w900, color: AppTheme.grey)),
    );
  }
}
