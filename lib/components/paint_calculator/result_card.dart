import 'package:flutter/material.dart';
import 'package:paint_calculator/theme.dart';

class ResultCard extends StatelessWidget {
  final Map<num, int> paintCans;

  const ResultCard({
    Key? key,
    required this.paintCans,
  }) : super(key: key);

  List<Widget> generateResults() {
    List<Widget> results = [];

    paintCans.forEach((key, value) {
      results.add(
        Row(
          children: [
            Flexible(
                child: Center(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text(
                  '$value x ${key.toStringAsFixed(2)}L',
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ))
          ],
        ),
      );
    });

    return results;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 10.0),
      decoration: BoxDecoration(
        color: AppTheme.ligthBlue,
        boxShadow: const [
          BoxShadow(
            color: Colors.grey,
            offset: Offset(0.0, 1.0), //(x,y)
            blurRadius: 6.0,
          ),
        ],
      ),
      child: Stack(
        children: [
          Row(
            children: [
              Container(
                decoration: const BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(10.0),
                        topRight: Radius.circular(10.0))),
                padding: const EdgeInsets.all(8.0),
                child: const Text(
                  'RESULT',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 13,
                      color: Colors.white),
                ),
              ),
            ],
          ),
          Column(
            children: [
              ...generateResults(),
            ],
          )
        ],
      ),
    );
  }
}
