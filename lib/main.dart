import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:paint_calculator/views/paint_calculator.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Paint Calculator',
      theme: ThemeData(
          textTheme:
              GoogleFonts.titilliumWebTextTheme(Theme.of(context).textTheme)),
      home: PaintCalculatorView(),
    );
  }
}
