import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:paint_calculator/components/paint_calculator/result_card.dart';
import 'package:paint_calculator/components/shared/base.dart';
import 'package:paint_calculator/components/shared/buttom.dart';
import 'package:paint_calculator/components/shared/input.dart';
import 'package:paint_calculator/controllers/paint_calculator/paint_calculator.dart';
import 'package:paint_calculator/controllers/shared/input.dart';

class PaintCalculatorView extends StatelessWidget {
  final controller = PaintCalculatorController();
  final labelTextStyle =
      const TextStyle(fontSize: 16, fontWeight: FontWeight.bold);

  PaintCalculatorView({Key? key}) : super(key: key);

  List<Widget> generateWalls(double width) {
    List<Widget> widgets = [];

    for (var i = 0; i < controller.walls.length; i++) {
      var wall = controller.walls[i];
      var wallInputs = generateWallInput(width, wall, 'Wall ${i + 1}');
      widgets = [...widgets, ...wallInputs];
    }

    return widgets;
  }

  List<Widget> generateWallInput(double width, WallData wall, String text) {
    return [
      Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Text(text, style: labelTextStyle),
      ),
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Row(
                children: [
                  Expanded(
                      child: Input(
                          validate: controller.validate,
                          hintText: 'WIDTH',
                          controller: wall.widthController,
                          type: TextInputType.number)),
                  const SizedBox(width: 8),
                  Expanded(
                      child: Input(
                          validate: controller.validate,
                          hintText: 'HEIGHT',
                          controller: wall.heightController,
                          type: TextInputType.number))
                ],
              ),
            ),
          ],
        ),
      ),
      Row(
        children: [
          generateNoPaintInput(width, wall.windowController, 'WINDOW'),
          const SizedBox(width: 8),
          generateNoPaintInput(width, wall.doorController, 'DOOR'),
        ],
      ),
      Row(
        children: const [
          Expanded(
              child: Divider(
            thickness: 2,
          )),
        ],
      ),
    ];
  }

  generateNoPaintInput(
      double width, InputController inputController, String text) {
    return Expanded(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(text, style: labelTextStyle),
            SizedBox(
              width: width * 0.25,
              child: Input(
                validate: controller.validateDoorWall,
                controller: inputController,
                type: TextInputType.number,
                hintText: 'QTY',
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Base(
        screenName: 'PAINT CALCULATOR',
        body: Padding(
          padding: const EdgeInsets.only(left: 15.0, right: 15.0, bottom: 15.0),
          child: Form(
            key: controller.formKey,
            child: Column(
              children: [
                ...generateWalls(width),
                Observer(builder: (_) {
                  if (controller.errorMessage == null) {
                    return Container();
                  } else {
                    return Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                        '${controller.errorMessage}',
                        style: const TextStyle(color: Colors.red, fontSize: 16),
                      ),
                    );
                  }
                }),
                Row(
                  children: [
                    Expanded(
                        child: Buttom(
                            buttomText: 'CALCULATE',
                            buttomAction: controller.calculate)),
                  ],
                ),
                Observer(builder: (_) {
                  if (controller.paintResults.isEmpty) {
                    return Container();
                  } else {
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Expanded(
                          child:
                              ResultCard(paintCans: controller.paintResults)),
                    );
                  }
                  return ResultCard(paintCans: controller.paintResults);
                })
              ],
            ),
          ),
        ));
  }
}
