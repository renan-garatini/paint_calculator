import 'package:flutter/material.dart';

class AppTheme {
  static var orange = const Color(0xffF05423);
  static var green = const Color(0xff30BF0E);
  static var grey = const Color(0xffC8C8C8);
  static var ligthBlue = const Color(0xff88D8F2);
}
